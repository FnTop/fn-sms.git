package cn.fntop.config;

import cn.fntop.entity.SmsMessage;
import cn.fntop.entity.SmsRequest;
import cn.fntop.entity.SmsResponse;
import cn.fntop.service.OpenApi;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SmsApi extends OpenApi {
    @POST("core/send")
    Single<SmsMessage<SmsResponse>> send(@Body SmsRequest request);
}