package cn.fntop.entity;

import lombok.*;

import java.io.Serializable;

/**
 * @author fn
 * @description
 * @date 2023/7/22 23:02
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsResponse implements Serializable {

    private static final long serialVersionUID = 83561965198613662L;
    private String phone;
    private String content;
    private String callData;
    private Long msgId;
    private Long smsCount;
}
