package cn.fntop.entity;

import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * 响应信息主体
 */
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SmsMessage<SmsResponse> implements Serializable {
    private static final long serialVersionUID = 7771518076151772686L;
    private String code;
    private String msg;
    private List<SmsResponse> data;


}
